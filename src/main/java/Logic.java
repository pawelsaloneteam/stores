import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Logic {
    private Map<String, Store> stores;
    private Scanner input;

    public Logic() {
        stores = new HashMap<>();
        stores.put("internetowy", new EStore("internetowy"));
        stores.put("Wiertlo", new StoreStationary("Wiertlo"));
        stores.put("Tadek", new StoreSationaryTelephone("Tadek"));
        input= new Scanner(System.in);
    }

    public void printStores(){
        System.out.println("Sellers:");
        for(String sellerName : stores.keySet()){
            System.out.println(sellerName);
        }

        System.out.println("\n\n");


    }

    public void chooseStore(){
        System.out.println("Type in seller name.");
        String seller = input.next();
        System.out.println("\n");

        if(stores.containsKey(seller)) {
            System.out.println(seller + ":\n" +
                    "price for products: " + Double.toString(stores.get(seller).getPrice()));
            stores.get(seller).printStepsToBuy();
        }else{
            System.out.println("Store not found.");
        }

        System.out.println("\n\n");
    }

}
