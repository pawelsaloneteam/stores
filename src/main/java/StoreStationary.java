public class StoreStationary implements Store {
    private String name;
    private String[] steps;
    private double price;

    public StoreStationary(String name) {
        this.name = name;
        steps = new String[]{"Go to the store."};
        price = 40.9;
    }

    public double getPrice() {
        return price;
    }

    public void printStepsToBuy() {
        System.out.println("Steps to Buy:");
        for(String step : steps){
            System.out.println(step);
        }
    }

    public void goToTheStore() {
        System.out.println("I walk to the store "+ name);
    }
}
