public interface Store{
    public double getPrice();
    public void printStepsToBuy();
}
