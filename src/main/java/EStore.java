public class EStore implements Store {
    private String name;
    private String[] steps;
    private double price;

    public EStore(String name) {
        this.name = name;
        steps = new String[]{"Internet order", "Receiving email with invoice", "make a appointment with courier"};
    }

    public double getPrice() {
        return price;
    }

    public void printStepsToBuy() {
        System.out.println("Steps to Buy:");
        for(String step : steps){
            System.out.println(step);
        }
    }

    public void makeOrder(String product) {

    }

    public String getInvoiceEmail() {
        return null;
    }

    public void makeAppointmentWithCourier() {

    }
}
