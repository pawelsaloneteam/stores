public class StoreSationaryTelephone implements Store {

    private double price;
    private String[] steps;
    private String name;

    public StoreSationaryTelephone(String name) {
        this.name=name;
        steps = new String[]{"Make Phone Order", "Go to the Store"};
        price = 99.03;
    }

    public void makePhoneOrder(String product) {
        System.out.println("I make phone order in store "+ name);
    }

    public void goToTheStore() {
        System.out.println("I walk to the store " + name);
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void printStepsToBuy() {
        System.out.println("Steps to Buy:");
        for(String step : steps){
            System.out.println(step);
        }
    }
}
