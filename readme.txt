Program przedstawia rozwiązanie zadania wykonywanego w ramach kursu programowania Java.

 Mamy firmę, która kupuje jakieś produkty od sprzedawcy. Sprzedawców może być wielu, mogą mieć
 różne ceny i wymagać różych działań … trudno przewidzieć jakich.
 Na razie mamy trzech sprzedawców: sklep internetowy, sklep Wiertło oraz sklep Tadek.
 Sklep internetowy wymaga: zamówienia internetowego, odebrania maila z fakturą, zaplanowanie
 odebrania towaru od kuriera.
 Sklep Wiertło wymaga: podróży do sklepu.
 Sklep Tadek wymaga: zamówienia telefonicznego, podróży do sklepu.
 Celem aplikacji jest wypisanie powyższych kroków jakie musimy uczynić by kupić towar z możliwością
 swobodnej zmiany sprzedawcy.
 Napisać klasę Simulation, która pokaże swobodę zmiany sprzedawcy i wypisze cenę oraz wymagane
 kroki.
